# Hubble

1. I implemented a sort of dynamic placeholder variable substitution, so SUBSTITUTION_MAP contains the keys to be updated by the product
of values passed from command line. Might be in a real project would be better to pass SUBSTITUTION_MAP instead of keeping it as a constant
2. Though might be not fully implemented here, but we always should try to sanitize user input - i.e. see ```HubbleCli.__sanitize_file_name```
3. I selected Python cause for tasks like that it's quite simple and Python interpreter exists on all systems. 
4. In real project there probably could be more test added and more edge cases to cover
5. Also I tested private methods and I understand it is might be the not best approach, but an idea is that sometimes 
it's easier to cover something in unit tests comparing to validating results in higher level tests like ```test_process()``` 
7. For sure it's possible to use bash with the help of the approaches like:
https://unix.stackexchange.com/questions/97814/array-cartesian-product-in-bash
https://stackoverflow.com/questions/415677/how-to-replace-placeholders-in-a-text-file