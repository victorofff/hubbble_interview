import json
import os.path
from unittest import TestCase

from hubble_cli.hubble import HubbleCli
from hubble_cli.util import Utils


class TestHubbleCli(TestCase):

  def test_resolve_file_name(self):
    hubble = HubbleCli(False)
    file_name = hubble._HubbleCli__resolve_file_name(["a", "bb", "c2"], "parameter1")
    self.assertEquals("a_bb_c2_parameter1.json", file_name)

  def test_sanitize_file_name(self):
    hubble = HubbleCli(False)
    file_name = hubble._HubbleCli__sanitize_file_name("dummy .json")
    self.assertEquals("dummy-json", file_name)

  def test_get_substitution_combination(self):
    hubble = HubbleCli(False)

    source_dict = {
      "accounts": ["123", "456", "789"],
      "regions": ["us-east-1", "us-east-2"]
    }
    sub_keys, products = file_name = hubble._HubbleCli__get_substitution_combinations(source_dict)
    self.assertEqual(sorted(sub_keys), sorted(["account", "region"]))
    self.assertEqual(sorted(products), sorted([("123", "us-east-1"), ("123", "us-east-2"), ("456", "us-east-1"), ("456", "us-east-2"), ("789", "us-east-1"), ("789", "us-east-2")]))

  def test_process(self):
    hubble = HubbleCli(False)

    arg_parser = Utils.create_parser()
    args = arg_parser.parse_args(["-p" "files/parameters1.json", "-a", "123,456,789", "-r", "us-east-1,us-east-2", "--output",  "/tmp"])

    hubble.process(args)
    self.__check_target_file_valid("123", "us-east-1", "parameters1", "/tmp")
    self.__check_target_file_valid("456", "us-east-1", "parameters1", "/tmp")
    self.__check_target_file_valid("789", "us-east-1", "parameters1", "/tmp")

    self.__check_target_file_valid("123", "us-east-2", "parameters1", "/tmp")
    self.__check_target_file_valid("456", "us-east-2", "parameters1", "/tmp")
    self.__check_target_file_valid("789", "us-east-2", "parameters1", "/tmp")


  def __check_target_file_valid(self, account: str, region: str, parameter_name: str, parent_folder: str):
    path = os.path.join(parent_folder, "{}_{}_{}.json".format(account, region, parameter_name))
    with open(path) as f:
      data = json.load(f)
      self.assertEqual(account, data["parameters"]["app"]["account"])
      self.assertEqual(region, data["parameters"]["app"]["region"])
