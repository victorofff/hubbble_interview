import argparse

class Utils:

  @staticmethod
  def list_str(values):
    if values is None or len(values) == 0:
      raise RuntimeError("Invalid data")
    list_values = values.split(',')
    return [s.strip() for s in list_values]

  @staticmethod
  def create_parser():

    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--parameters", dest="parameters", type=str, required=True,
                        help="Parameters json file")
    parser.add_argument("-a", "--accounts", dest="accounts", type=Utils.list_str, required=True,
                        help="Accounts comma separated list")
    parser.add_argument("-r", "--regions", dest="regions", type=Utils.list_str, required=True,
                        help="Regions comma separated list")
    parser.add_argument("-o", "--output", dest="output", type=str, default=".", required=False,
                        help="Output directory")
    parser.add_argument("-v", "--verbose", action='store_true', required=False, help="Verbose mode")

    return parser
