from hubble import HubbleCli
from util import Utils

if __name__ == '__main__':
  args = Utils.create_parser().parse_args()
  hubble_cli = HubbleCli(args.verbose)
  try:
    hubble_cli.process(args)
  except Exception as ex:
    hubble_cli.get_logger().critical(ex, exc_info=True)
