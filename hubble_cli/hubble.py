import copy
import itertools
import json
import logging
import os
import re
import unicodedata


LOGGER_NAME = "hubble_cli"
LOG_FILE_NAME = "out.log"
SUBSTITUTION_MAP = {
  "account": "accounts",
  "region": "regions"
}

#ideally we can do recursive calls but this is ok for now
CHANGE_MAP_RESOLVER = lambda json: json["parameters"]["app"]

class HubbleCli:
  def __init__(self, verbose):
    self.__init_logging(verbose)
    self.logger = logging.getLogger(LOGGER_NAME)

  def __init_logging(self, verbose):
    logging.basicConfig(
      level=logging.DEBUG if verbose else logging.INFO,
      format="%(name)s: %(asctime)s [%(levelname)s] %(message)s",
      handlers=[
        # logging.FileHandler(LOG_FILE_NAME),
        logging.StreamHandler()
      ]
    )

  def __sanitize_file_name(self, src_file: str) -> str:
    if src_file is None or len(src_file) == 0:
      raise RuntimeError("Invalid file")

    value = str(src_file)
    value = unicodedata.normalize("NFKC", value)
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")

  def __resolve_file_name(self, prefixes: list, parameters_path: str) -> str:
    if len(prefixes) == 0:
      raise RuntimeError("Prefix list cannot be empty")
    parameters_end_name = os.path.splitext(os.path.basename(parameters_path))[0]
    return self.__sanitize_file_name(
      "".join(["_{}".format(prefix) for prefix in prefixes]).strip("_") + "_" + parameters_end_name) + ".json"

  def __get_substitution_combinations(self, param_dict: dict):
    source_list = []
    for key, value in SUBSTITUTION_MAP.items():
      values = param_dict[value]
      source_list.append(values)

    products = list(itertools.product(*source_list))
    sub_keys = list(SUBSTITUTION_MAP.keys())

    return sub_keys, products

  # can resolve dynamically but just hard-coded for simplicity
  # no check for errors as we expect the contract
  def __get_working_entry(self, json: dict):
    return CHANGE_MAP_RESOLVER(json)

  def __process_json(self, dst_dir: str, src_filename: str, sub_keys: list, products: list):
    with open(src_filename, "r") as parameters_file:
      json_data = json.load(parameters_file)

      for i, attributes in enumerate(products):
        json_copied_data = copy.copy(json_data)
        for j, new_value in enumerate(attributes):

          if (self.logger.isEnabledFor(logging.DEBUG)):
            self.logger.debug("Changing key [{}] to new value [{}]".format(sub_keys[j], new_value))

          json_to_change = self.__get_working_entry(json_copied_data)

          #so far assume the keys are always valid
          json_to_change[sub_keys[j]] = new_value

        self.__save_to_output(dst_dir, json_copied_data, attributes, src_filename)

  def __save_to_output(self, dst_dir: str, json_copied_data: dict, attributes: list, parameters_path: str):
    content = json.dumps(json_copied_data, indent=2)
    dst_file = os.path.join(dst_dir, self.__resolve_file_name(attributes, parameters_path))
    if (self.logger.isEnabledFor(logging.DEBUG)):
      self.logger.debug("Writing info to {}".format(dst_file))
    with open(dst_file, "w") as dst_file_handler:
      dst_file_handler.write(content)

  def get_logger(self):
    return self.logger

  def process(self, args):
    self.logger.info("Started, processing input file: {}".format(args.parameters))

    param_dict = vars(args)
    sub_keys, products = self.__get_substitution_combinations(param_dict)
    self.__process_json(args.output, args.parameters, sub_keys, products)

    self.logger.info("Finished")
